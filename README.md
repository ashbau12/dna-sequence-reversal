This project is an application that will allow you to reverse the order in which
codons occur in a DNA sequence. Rather than reversing the sequence, it will 
reverse the sequence three nucleotides at a time, as to maintain the same amino
acids in the transcribed protein, however connected in the reverse order.
